import pygtk
pygtk.require("2.0")
import gtk
import gtk.glade
 
def filechooser_create():
    return gtk.FileChooserWidget(action=gtk.FILE_CHOOSER_ACTION_OPEN, backend=None)
 
gtk.glade.set_custom_widget_callbacks(locals())
glade = gtk.glade.XML("burn.glade")
window = glade.get_widget ("wnd_main_vert")
window.show_all()
gtk.main()
