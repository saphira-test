import pygtk
pygtk.require('2.0')
import gtk
import gtk.glade

class Gui:
    def __init__(self):
        gladefile = "burn.glade"
        self.windowname = "dlg_start"
        self.wTree = gtk.glade.XML(gladefile, self.windowname)
        self.wTree.get_widget(self.windowname).show()
        self.wTree.signal_autoconnect(self)
        self.exp_width, self.exp_height = self.wTree.get_widget(self.windowname).get_default_size()
        self.tabpane = self.wTree.get_widget("notebook2")
        self.tabpane.set_show_tabs(False)

    def on_quit_clicked(self, widget, udata=None):
        gtk.main_quit()

    def on_btn_advanced_toggled(self, widget, udata=None):
        if widget.get_active():
            self.tabpane.set_current_page(1)
        else:
            self.tabpane.set_current_page(0)

w = Gui()
gtk.main()
