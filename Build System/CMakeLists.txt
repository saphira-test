#==============================================================================
# CMake toplevel build file for Saphira
#
# Author:       Andreas Kühntopf <andreas@kuehntopf.org>
#
# Description:  This file handles the Saphira build process
#               It defines the Project, adds definitions
#               and checks for missing dependencies.
# Version:      0.1
#==============================================================================


#==============================================================================
# Import modules needed by this script
#==============================================================================
SET( CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake )
INCLUDE(FindPkgConfig)
INCLUDE(FindGettext)


#==============================================================================
# Project definition
#==============================================================================
PROJECT(saphira)

# Set defines which can be used in the code
SET(VERSION "0.0.0")
SET(APPNAME "saphira")

ADD_DEFINITIONS(-DVERSION="\\"${VERSION}\\"")
ADD_DEFINITIONS(-DAPPNAME="\\"${APPNAME}\\"")


#==============================================================================
# Dependency checks
#==============================================================================
# Need to unset the __pkg_config variables,
# so that it keeps checking on executing cmake .
# Otherwise it might generate the Makefile despite
# the missing dependencies.

SET (__pkg_config_checked_GLIB-2.0 "0")
pkg_check_modules(GLIB-2.0 REQUIRED glib-2.0)
INCLUDE_DIRECTORIES(${GLIB-2.0_INCLUDE_DIRS})
LINK_LIBRARIES(${GLIB-2.0_LIBRARIES})

SET (__pkg_config_checked_GTK2 "0")
pkg_check_modules(GTK2 REQUIRED gtk+-2.0>=2.10)
INCLUDE_DIRECTORIES(${GTK2_INCLUDE_DIRS})
LINK_LIBRARIES(${GTK2_LIBRARIES})

SET (__pkg_config_checked_LIBGLADE-2.0 "0")
pkg_check_modules(LIBGLADE-2.0 REQUIRED libglade-2.0)
INCLUDE_DIRECTORIES(${LIBGLADE-2.0_INCLUDE_DIRS})
LINK_LIBRARIES(${LIBGLADE-2.0_LIBRARIES})

SET (__pkg_config_checked_LIBBURN "0")
pkg_check_modules(LIBBURN REQUIRED libburn-5)
INCLUDE_DIRECTORIES(${LIBBURN_INCLUDE_DIRS})
LINK_LIBRARIES(${LIBBURN_LIBRARIES})

SET (__pkg_config_checked_LIBISOFS "0")
pkg_check_modules(LIBISOFS REQUIRED libisofs-5)
INCLUDE_DIRECTORIES(${LIBISOFS_INCLUDE_DIRS})
LINK_LIBRARIES(${LIBISOFS_LIBRARIES})

SET (__pkg_config_checked_LIBXML-2.0 "0")
pkg_check_modules(LIBXML-2.0 REQUIRED libxml-2.0)
INCLUDE_DIRECTORIES(${LIBXML-2.0_INCLUDE_DIRS})
LINK_LIBRARIES(${LIBXML-2.0_LIBRARIES})

SET (__pkg_config_checked_GSTREAMER-0.10 "0")
pkg_check_modules(GSTREAMER-0.10 REQUIRED gstreamer-0.10)
INCLUDE_DIRECTORIES(${GSTREAMER-0.10_INCLUDE_DIRS})
LINK_LIBRARIES(${GSTREAMER-0.10_LIBRARIES})


#==============================================================================
# Custom variables
#==============================================================================
# Default installation prefix is /usr
SET(CMAKE_INSTALL_PREFIX /usr)


#==============================================================================
# Which subdirectories should also be processed?
#==============================================================================
ADD_SUBDIRECTORY(src build)
ADD_SUBDIRECTORY(po)
