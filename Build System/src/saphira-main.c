#include <stdio.h>
#include <libintl.h>

#define _(String) gettext(String)

int
main(int argc, char *argv[])
{
    printf(_("Hello, World!\n"));
    return 0;
}
