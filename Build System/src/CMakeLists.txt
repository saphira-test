#==============================================================================
# CMake source build file for Saphira
#
# Author:       Andreas Kühntopf <andreas@kuehntopf.org>
#
# Description:  This file handles the Saphira build process
#               It generates a binary from the given source files
#
# Version:      0.1
#==============================================================================

# Add Program sources here
SET(PROGRAM_SOURCES
	
    saphira-main.c

)

ADD_EXECUTABLE(saphira ${PROGRAM_SOURCES})

INSTALL(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/saphira
	DESTINATION bin)

